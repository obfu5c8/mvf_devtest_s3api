<?php
require '../vendor/autoload.php';

use MVF\API\Cache\Cache;
use MVF\API\Cache\RedisCache;
use MVF\API\Datastore\Datastore;
use MVF\API\Datastore\DatastoreImpl;
use MVF\API\Util\Env;


ini_set('expose_php', false);


$app = new \MVF\API\App([
    'settings' => [
        'displayErrorDetails' => true,
    ]
]);


// Configure the cache layer
$redisHost = Env::get('MVF_REDIS_HOST', '127.0.0.1');
Cache::setImplementation(new RedisCache($redisHost));


// Configure the datastore for accessing the data
$dbHost = Env::get('MVF_MYSQL_HOST', '127.0.0.1');
Datastore::setImplementation(new DatastoreImpl($dbHost,
    'mvf_api_data',
    'api_user',
    'password'));


// Process the request
$app->run();