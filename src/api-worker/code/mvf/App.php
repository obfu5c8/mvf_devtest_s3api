<?php
/**
 * @copyright 2017 Alan Pich <alan.pich@gmail.com>
 */

namespace MVF\API;


use MVF\API\ContentTypes\JSONContentType;
use MVF\API\ContentTypes\JSONPContentType;
use MVF\API\ContentTypes\YAMLContentType;
use MVF\API\Http\Response;

use MVF\API\Middleware\ContentTypeHandler;
use MVF\API\Middleware\ResponseBrandingHandler;

use MVF\API\Resources\Accounts\AccountResource;
use MVF\API\Resources\Base\IResource;
use MVF\API\Resources\CustomerAccounts\CustomerAccountsResource;
use MVF\API\Util\Env;


/**
 * Class App
 *
 * Extends the default Slim app to add in our middleware and endpoints.
 *
 * @package MVF\API
 */
class App extends \Slim\App
{
    /**
     * App constructor.
     *
     * @param array $container
     */
    public function __construct($container = [])
    {
        // Supply our own custom Response class to use instead of Slim's default
        $container['response'] = new Response();

        parent::__construct($container);


        // Sets the 'Server' response header to something sexy
        $this->add(new ResponseBrandingHandler());


        // Authorisation handler checks requests are authorised
        /** @noinspection PhpUnusedLocalVariableInspection */
        $secretKey = Env::get('MVF_SECRET_KEY', '8acfab26-1efb-4d3f-9701-bb4743d62d71');
//        $this->add(new AuthenticationHandler($secretKey));


        // API supports many content types
        $this->add(new ContentTypeHandler([
            new JSONContentType(),
            new YAMLContentType(),
            new JSONPContentType()
        ]));


        // Register the API resource endpoints
        $this->registerResource('/customer/{customer_guid}/accounts', new CustomerAccountsResource());
        $this->registerResource('/accounts/{account_guid}', new AccountResource());
    }


    /**
     * Register a resource to this app at a particular endpoint
     *
     * @param string    $path     Path to register resource at
     * @param IResource $resource The resource instance to be registered
     */
    public function registerResource(string $path, IResource $resource)
    {
        $resource->register($this, $path);
    }


}