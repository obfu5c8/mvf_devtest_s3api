<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Cache;

use Psr\SimpleCache\CacheInterface;


/**
 * Class Cache
 *
 * @package MVF\API\Cache
 */
class Cache
{

    /**
     * @var CacheInterface
     */
    private static $cache;

    /**
     * @param CacheInterface $cache
     */
    public static function setImplementation(CacheInterface $cache)
    {
        self::$cache = $cache;
    }

    /**
     * Retrieve an item from the cache
     *
     * @param string $type
     * @param string $key
     * @return mixed
     */
    public static function get(string $type, string $key)
    {
        return self::$cache->get(self::fullKey($type, $key));
    }

    /**
     * Set an item into the cache
     *
     * @param string $type
     * @param string $key
     * @param mixed  $obj
     */
    public static function set($type, $key, $obj)
    {
        return self::$cache->set(self::fullKey($type, $key), $obj);
    }


    /**
     * @param string $type
     * @param string $key
     *
     * @return string
     */
    protected static function fullKey(string $type, string $key)
    {
        return $type . '::' . $key;
    }

}