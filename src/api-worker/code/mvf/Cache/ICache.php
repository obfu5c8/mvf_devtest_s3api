<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Cache;


/**
 * Interface ICache
 *
 * @package MVF\API\Cache
 */
interface ICache
{
    /**
     * Retrieve an object from the cache, returning null
     * if it does not exist
     *
     * @param string $type Group/Bucket the object lives in
     * @param string $key  Cache key
     *
     * @return mixed
     */
    public function get($type, $key);

    /**
     * Put an object into the cache
     *
     * @param string $type Group/Bucket the object lives in
     * @param string $key  Cache key
     * @param mixed  $obj  The object to store
     */
    public function set($type, $key, $obj);
}