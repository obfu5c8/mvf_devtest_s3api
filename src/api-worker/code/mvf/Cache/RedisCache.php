<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Cache;

use Psr\SimpleCache\CacheInterface;


/**
 * Class RedisCache
 *
 * @package MVF\API\Cache
 */
class RedisCache implements CacheInterface
{

    /** @var  \Predis\Client */
    protected $redis;


    /**
     * RedisCache constructor.
     *
     * @param string $host
     * @param int    $port
     * @param string $scheme
     */
    public function __construct(string $host = '127.0.0.1', int $port = 6379, string $scheme = 'tcp')
    {
        $this->redis = new \Predis\Client([
            'scheme' => $scheme,
            'host' => $host,
            'port' => $port
        ]);
    }


    /**
     * Retrieve an object from the cache, returning null
     * if it does not exist
     *
     * @param string $key Cache key
     *
     * @param null   $default
     *
     * @return mixed
     */
    public function get($key, $default=null)
    {
        $value = $this->redis->get($key);
        if ($value) {
            return \unserialize($value);
        }
        return null;
    }

    /**
     * Put an object into the cache
     *
     * @param string $key   Cache key
     * @param mixed  $value The object to store
     *
     * @param null   $ttl
     *
     * @return bool|void
     */
    public function set($key, $value, $ttl=null)
    {
        $value = serialize($value);
        $this->redis->set($key, $value);
    }


    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function delete($key)
    {
        $this->redis->del([$key]);
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear()
    {
        $this->redis->flushall();
        return true;
    }

    /**
     * Obtains multiple cache items by their unique keys.
     *
     * @param iterable $keys    A list of keys that can obtained in a single operation.
     * @param mixed    $default Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function getMultiple($keys, $default = null)
    {
        // TODO: Implement getMultiple() method.
        return [];
    }

    /**
     * Persists a set of key => value pairs in the cache, with an optional TTL.
     *
     * @param iterable              $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|DateInterval $ttl    Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $values is neither an array nor a Traversable,
     *   or if any of the $values are not a legal value.
     */
    public function setMultiple($values, $ttl = null)
    {
        foreach($values as $key=>$value) {
            $this->set($key, $value, $ttl);
        }
    }

    /**
     * Deletes multiple cache items in a single operation.
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function deleteMultiple($keys)
    {
        $this->redis->del($keys);
    }

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function has($key)
    {
        return $this->redis->exists($key);
    }
}