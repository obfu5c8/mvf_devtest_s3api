<?php
/**
 * @copyright (c) 2017, Alan Pich
 */

namespace MVF\API\Combinations;

class Combination
{

    protected $entries;

    public function __construct( $entries = [] )
    {
        $this->entries = $entries;
    }

    public function getEntries ()
    {
        return $this->entries;
    }

    public function getSum() {
        return \array_sum( array_map(function(Entry $e){
            return $e->getBalance();
        }, $this->entries));
    }

    public function getSize(){
        return sizeof($this->entries);
    }


}