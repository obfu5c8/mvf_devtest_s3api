<?php
/**
 * @copyright (c) 2017, Alan Pich
 */

namespace MVF\API\Combinations;

class Entry
{
    protected $guid;
    protected $balance;

    public function __construct( $guid, $balance )
    {
        $this->guid = $guid;
        $this->balance = $balance;
    }


    public function getBalance()
    {
        return $this->balance;
    }

    public function getGuid()
    {
        return $this->guid;
    }


}