<?php
/**
 * @copyright (c) 2017, Alan Pich
 */

namespace MVF\API\Combinations;

use JsonSerializable;

class Result implements JsonSerializable
{

    public $numAccounts;
    public $accounts;
    public $totalBalance;
    public $iterationsRequired;
    public $timeTaken;

    public function __construct($entries, $iterations=0, $time=0)
    {
        $this->accounts = array_map(function(Entry $e){
            return $e->getGuid();
        }, $entries);

        $this->totalBalance = array_sum(
            array_map( function(Entry $e){
                return $e->getBalance();
            }, $entries )
        );

        $this->numAccounts = sizeof($this->accounts);
        $this->iterationsRequired = $iterations;
        $this->timeTaken = $time;
    }


    /**
     * Specify data which should be serialized to JSON
     *
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'numAccounts' => $this->numAccounts,
            'accounts' => $this->accounts,
            'totalBalance' => $this->totalBalance,
            'iterationsRequired' => $this->iterationsRequired,
            'timeTaken' => $this->timeTaken,
            'memoryPeak' => round(memory_get_peak_usage (true) / 1024 / 1024,2)."MB"
        ];
    }
}