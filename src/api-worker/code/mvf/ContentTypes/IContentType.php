<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\ContentTypes;

use Slim\Http\Request;

/**
 * Interface IContentType
 *
 * @package MVF\API\ContentTypes
 */
interface IContentType
{
    /**
     * Returns the MIME type for this type
     *
     * @return string
     */
    public function getMimeType(): string;

    /**
     * Returns the format key for this type
     *
     * @return string
     */
    public function getFormatKey(): string;

    /**
     * Encode a data structure into a string representation
     *
     * @param array   $data    The data to encode
     * @param Request $request The HTTP request
     *
     * @return string
     */
    public function encode($data, Request $request): string;
}