<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\ContentTypes;

use Slim\Http\Request;

/**
 * Class JSONContentType
 *
 * @package MVF\API\ContentTypes
 */
class JSONContentType implements IContentType
{

    /**
     * Returns the MIME type for this type
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return 'application/json';
    }

    /**
     * Returns the format key for this type
     *
     * @return string
     */
    public function getFormatKey(): string
    {
        return 'json';
    }

    /**
     * Encode a data structure into a string representation
     *
     * @param array   $data    The data to encode
     * @param Request $request The HTTP request
     *
     * @return string
     */
    public function encode($data, Request $request): string
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }
}