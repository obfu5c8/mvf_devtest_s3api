<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\ContentTypes;


use Slim\Http\Request;

/**
 * Class JSONPContentType
 *
 * @package MVF\API\ContentTypes
 */
class JSONPContentType implements IContentType
{

    /**
     * Returns the MIME type for this type
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return 'application/javascript';
    }

    /**
     * Returns the format key for this type
     *
     * @return string
     */
    public function getFormatKey(): string
    {
        return 'jsonp';
    }

    /**
     * Encode a data structure into a string representation
     *
     * @param array   $data    The data to encode
     * @param Request $request The HTTP request
     *
     * @return string
     */
    public function encode($data, Request $request): string
    {
        $funcName = $request->getQueryParam('@callback', 'callback');
        $cbId = $request->getQueryParam('@callback-id', null);
        $encodedData = json_encode($data, JSON_PRETTY_PRINT);

        $javascript = $funcName . "(";

        if ($cbId) {
            $javascript .= "'" . $cbId . "', ";
        }
        $javascript .= $encodedData . ");";

        return $javascript;
    }
}