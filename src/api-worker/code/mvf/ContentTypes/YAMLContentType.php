<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\ContentTypes;

use Slim\Http\Request;
use Symfony\Component\Yaml\Yaml;


/**
 * Class YAMLContentType
 *
 * @package MVF\API\ContentTypes
 */
class YAMLContentType implements IContentType
{

    /**
     * Returns the MIME type for this type
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return 'application/yaml';
    }

    /**
     * Returns the format key for this type
     *
     * @return string
     */
    public function getFormatKey(): string
    {
        return 'yaml';
    }

    /**
     * Encode a data structure into a string representation
     *
     * @param array   $data    The data to encode
     * @param Request $request The HTTP request
     *
     * @return string
     */
    public function encode($data, Request $request): string
    {
        if($data instanceof \JsonSerializable){
            $data = $data->jsonSerialize();
        }
        return Yaml::dump($data);
    }
}