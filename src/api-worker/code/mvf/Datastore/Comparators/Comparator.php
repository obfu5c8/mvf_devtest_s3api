<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore\Comparators;


/**
 * Class Comparator
 *
 * @package MVF\API\Datastore\Comparators
 */
abstract class Comparator
{
    /**
     * The field being compared
     *
     * @var string
     */
    protected $field;

    /**
     * The value to compare against
     *
     * @var string
     */
    protected $value;

    /**
     * Comparator constructor.
     *
     * @param string $fieldName
     * @param mixed  $value
     */
    public function __construct(string $fieldName, $value)
    {
        $this->field = $fieldName;
        $this->value = $value;
    }

    /**
     * Returns an SQL clause for `$this->field <comparator> $this->value`
     * If $tableName is supplied, then it is prefixed to $this->field
     * in the form `$tableName.$this->field`
     *
     * @param string $tableName
     *
     * @return string
     */
    public function getClause(string $tableName = null): string
    {
        $field = $tableName ? sprintf('`%s`.`%s`', $tableName, $this->field) : sprintf('`%s`', $this->field);
        return $this->getSQLClause($field, $this->value);
    }

    /**
     * Returns an SQL clause for `$field <comparator> $value`
     *
     * @param string $field Name of field to compare
     * @param mixed  $value Value to compare against
     *
     * @return string
     */
    abstract public function getSQLClause(string $field, $value): string;


}