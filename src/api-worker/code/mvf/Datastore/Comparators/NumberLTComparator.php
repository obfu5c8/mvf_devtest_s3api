<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore\Comparators;


/**
 * Class NumberLTComparator
 *
 * @package MVF\API\Datastore\Comparators
 */
class NumberLTComparator extends Comparator
{

    /**
     * Returns an SQL WHERE clause for $field < $value
     *
     * @param string $field
     * @param mixed $value
     *
     * @return string
     */
    public function getSQLClause(string $field, $value): string
    {
        return $field . ' < ' . $value;
    }
}