<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore\Comparators;


/**
 * Class StringContainsComparator
 *
 * @package MVF\API\Datastore\Comparators
 */
class StringContainsComparator extends Comparator
{

    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return string
     */
    public function getSQLClause(string $field, $value): string
    {
        return $field . ' LIKE \'%' . $value . '%\'';
    }
}