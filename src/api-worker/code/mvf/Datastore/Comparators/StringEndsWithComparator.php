<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore\Comparators;


/**
 * Class StringEndsWithComparator
 *
 * @package MVF\API\Datastore\Comparators
 */
class StringEndsWithComparator extends Comparator
{

    /**
     * Returns an SQL WHERE clause for $field->endsWith($value)
     *
     * @param $field
     * @param $value
     *
     * @return string
     */
    public function getSQLClause(string $field, $value): string
    {
        return $field . ' LIKE \'%' . $value . '\'';
    }
}