<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore\Comparators;


/**
 * Class StringStartsWithComparator
 *
 * @package MVF\API\Datastore\Comparators
 */
class StringStartsWithComparator extends Comparator
{

    /**
     * Returns an SQL WHERE clause for $field->startsWith($value)
     *
     * @param string $field
     * @param mixed  $value
     *
     * @return string
     */
    public function getSQLClause(string $field, $value): string
    {
        return $field . ' LIKE \'' . $value . '%\'';
    }
}