<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore;

use MVF\API\Datastore\Comparators\Comparator;


/**
 * Class Datastore
 *
 * @package MVF\API\Datastore
 */
class Datastore
{

    /** @var  IDataStore */
    private static $store;

    /**
     * Set the actual datastore implementation to use for fetching data
     *
     * @param IDataStore $store
     */
    public static function setImplementation(IDataStore $store)
    {
        self::$store = $store;
    }

    /**
     * Returns true if $customer_guid matches a customer in the store
     *
     * @param string $customer_guid
     *
     * @return bool
     */
    public static function doesCustomerExist($customer_guid): bool
    {
        return self::$store->doesCustomerExist($customer_guid);
    }

    /**
     * Fetches an array guid's for all accounts connected to the customer identified by $customer_guid.
     *
     * Optionally filter the results using an array of $comparators
     * Optionally sort the results using an array of sort fields
     *
     * @param string       $customer_guid
     * @param Comparator[] $comparators
     * @param array        $sort
     *
     * @return array|null
     */
    public static function getAccountsForCustomer(string $customer_guid, array $comparators = [], $sort = []): array
    {
        return self::$store->getAccountsForCustomer($customer_guid, $comparators, $sort);
    }

    /**
     * Fetches a single account identified by $account_guid as an associative array
     *
     * @param string $account_guid
     *
     * @return array
     */
    public static function getAccount(string $account_guid): array
    {
        return self::$store->getAccount($account_guid);
    }


    public static function getAccountBalancesForCustomerAccounts(string $customer_guid): array {
        return self::$store->getAccountBalancesForCustomerAccounts($customer_guid);
    }

}