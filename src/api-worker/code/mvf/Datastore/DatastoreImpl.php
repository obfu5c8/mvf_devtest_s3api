<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore;


use MVF\API\Cache\Cache;
use MVF\API\Datastore\Comparators\Comparator;
use PDO;

/**
 * Class DatastoreImpl
 *
 * @package MVF\API\Datastore
 */
class DatastoreImpl implements IDataStore
{

    /**
     * PDO instance for db queries.
     * Call getDB() to get this instance,
     * creating it if it does not exist
     *
     * @var PDO
     */
    private $db;
    /**
     * PDO dsn for database connection
     *
     * @var string
     */
    private $dsn;
    /**
     * Username to pass to PDO when connecting to db
     *
     * @var string
     */
    private $user;
    /**
     * Password to pass to PDO when connecting to db
     *
     * @var string
     */
    private $pass;


    /**
     * DatastoreImpl constructor.
     *
     * @param string $host DB hostname
     * @param string $name DB name
     * @param string $user DB username
     * @param string $pass DB password
     */
    public function __construct(string $host, string $name, string $user, string $pass)
    {
        $this->dsn = sprintf('mysql:host=%s;dbname=%s;charset=utf8mb4', $host, $name);
        $this->user = $user;
        $this->pass = $pass;
    }


    /**
     * Returns PDO instance for db queries
     *
     * @return PDO
     */
    public function getDB(): PDO
    {
        if ($this->db == null) {
            $this->db = new PDO($this->dsn, $this->user, $this->pass, array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ));
        }
        return $this->db;
    }


    /**
     * Returns true if $customer_guid matches a customer in the store
     *
     * @param string $customer_guid
     *
     * @return bool
     */
    public function doesCustomerExist(string $customer_guid): bool
    {
        try {

            // If it's in the cache, the customer exists
            $cachedData = Cache::get('customer_exists', $customer_guid);
            if ($cachedData != null) {
                return true;
            }

            $stmt = $this->getDB()->query(sprintf("
            SELECT 1 FROM `customers` WHERE `guid` = '%s' LIMIT 1
            ", $customer_guid));

            $exists = $stmt->rowCount() > 0;

            if ($exists) {
                Cache::set('customer_exists', $customer_guid, true);
            }

            return $exists;

        } catch (\PDOException $err) {
            throw $err;
        }
    }


    /**
     * Fetches an array GUIDs for all accounts connected to the customer identified by $customer_guid.
     *
     * Optionally filter the results using an array of $comparators
     * Optionally sort the results using an array of sort fields
     *
     * @param string       $customer_guid
     * @param Comparator[] $comparators
     * @param array        $sort
     *
     * @return array|null
     */
    public function getAccountsForCustomer(string $customer_guid, array $comparators = [], $sort = []): array
    {
        $sql = sprintf("
            SELECT 
                a.guid
            FROM	
                accounts a
            LEFT JOIN customer_accounts ca
                ON ca.account_guid = a.guid
            WHERE
                ca.customer_guid = '%s'
        ", $customer_guid);

        if ($comparators) {
            $sql .= " AND " . implode(' AND ', array_map(function (Comparator $c) {
                    return $c->getClause('a');
                }, $comparators));
        }

        if ($sort) {
            $sortClause = ' ORDER BY ' . implode(', ', $sort);
            $sql .= $sortClause;
        }


        // Generate a hash of the sql query for caching purposes
        $queryHash = hash('sha256', $sql);
        $cachedData = Cache::get('customer_account_query', $queryHash);
        if ($cachedData != null) {
            return $cachedData;
        }

        try {
            $stmt = $this->getDB()->query($sql);
            $data = $stmt->fetchAll(PDO::FETCH_COLUMN);

            if (sizeof($data) > 0) {
                Cache::set('customer_account_query', $queryHash, $data);
            }

            return $data;

        } catch (\PDOException $err) {
            //@TODO: Do we want to abstract the exceptions here?
            throw $err;
        }
    }


    public function getAccountBalancesForCustomerAccounts(string $customer_guid): array
    {
        $cachedData = Cache::get('customer_account_balances', $customer_guid);
        if ($cachedData != null) {
            return $cachedData;
        }

        $sql = sprintf("
            SELECT 
                a.guid as guid,
                a.balance AS balance
            FROM	
                accounts a
            LEFT JOIN customer_accounts ca
                ON ca.account_guid = a.guid
            WHERE
                ca.customer_guid = '%s'
        ", $customer_guid);

        try {
            $stmt = $this->getDB()->query($sql);
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (sizeof($data) > 0) {
                Cache::set('customer_account_balances', $customer_guid, $data);
            }

            return $data;

        } catch (\PDOException $err) {
            //@TODO: Do we want to abstract the exceptions here?
            throw $err;
        }

    }


    /**
     * Fetches a single account identified by $account_guid as an associative array
     *
     * Returns null if an account does not exist
     *
     * Caches db results for faster responses
     *
     * @param string $account_guid
     *
     * @return array|null
     */
    public function getAccount(string $account_guid): array
    {
        try {
            $cachedData = Cache::get('account', $account_guid);
            if ($cachedData != null) {
                return $cachedData;
            }

            //@TODO: Do a redis cache of db result here
            $stmt = $this->getDB()->query(sprintf("
                SELECT 
                  `guid`,
                  `firstname` AS firstname,
                  `lastname` AS lastname,
                  `email`,
                  `phone`,
                  `balance`,
                  `last_updated`
                FROM   `accounts`
                WHERE  `guid` = '%s'
                LIMIT 1
            ", $account_guid));

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($data != null) {
                Cache::set('account', $account_guid, $data);
            }

            return $data;


        } catch (\PDOException $err) {
            //@TODO: Do we want to abstract the exceptions here?
            throw $err;
        }
    }

}