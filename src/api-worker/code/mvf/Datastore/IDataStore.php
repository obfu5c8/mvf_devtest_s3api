<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Datastore;


use MVF\API\Datastore\Comparators\Comparator;

/**
 * Interface IDataStore
 *
 * @package MVF\API\Datastore
 */
interface IDataStore
{
    /**
     * Returns true if $customer_guid matches a customer in the store
     *
     * @param string $customer_guid
     *
     * @return bool
     */
    public function doesCustomerExist(string $customer_guid): bool;

    /**
     * Fetches an array guid's for all accounts connected to the customer identified by $customer_guid.
     *
     * Optionally filter the results using an array of $comparators
     * Optionally sort the results using an array of sort fields
     *
     * @param string       $customer_guid
     * @param Comparator[] $comparators
     * @param array        $sort
     *
     * @return array|null
     */
    public function getAccountsForCustomer(string $customer_guid, array $comparators = [], $sort = []): array;

    /**
     * Fetches a single account identified by $account_guid as an associative array
     *
     * @param string $account_guid
     *
     * @return array|null
     */
    public function getAccount(string $account_guid): array;


    public function getAccountBalancesForCustomerAccounts(string $customer_guid): array;
}