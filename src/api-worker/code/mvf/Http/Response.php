<?php
/**
 * @copyright 2017 Alan Pich
 */


namespace MVF\API\Http;


/**
 * Class Response
 *
 * @package MVF\API\Http
 */
class Response extends \Slim\Http\Response
{
    /**
     * Response data to be encoded into body before sending
     *
     * @var array
     */
    protected $data = null;

    /**
     * Adds an object to the response representing the data to be returned
     * in the response. This can then be encoded later on.
     *
     * @param mixed $data
     *
     * @return Response
     */
    public function withData($data = null): Response
    {
        $clone = clone $this;
        $clone->data = $data;
        return $clone;
    }

    /**
     * Return any data that has been previously attached to this response
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns true if data has been attached to this response
     *
     * @return bool
     */
    public function hasData(): bool
    {
        // WTF php?  [] === null
        if (is_array($this->data)) {
            return true;
        } else {
            return $this->data != null;
        }
    }


    /**
     * @param int $timestamp
     *
     * @return static
     */
    public function withLastModifiedTimestamp(int $timestamp)
    {

        $lm = gmdate('D M d Y H:i:s ', $timestamp) . 'GMT';
        return $this
            ->withHeader('Last-Modified', $lm);
    }

}