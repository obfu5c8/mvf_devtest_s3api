<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Middleware;

use MVF\API\Http\Response;
use MVF\API\Util\Auth;
use MVF\API\Util\Validate;
use Slim\Http\Request;


/**
 * Class AuthenticationHandler
 *
 * @package MVF\API\Middleware
 */
class AuthenticationHandler
{

    /**
     * Authentication secret key
     *
     * @var string
     */
    protected $secretKey;
    /**
     * Number of minutes slide a timestamp is considered valid for
     * when compared to server time
     *
     * @var int
     */
    protected $validMins;

    /**
     * AuthenticationHandler constructor.
     *
     * @param string $secretKey
     * @param int    $validMins
     */
    public function __construct(string $secretKey, int $validMins = 15)
    {
        $this->secretKey = $secretKey;
        $this->validMins = $validMins;
    }


    /**
     * Middleware invocation
     *
     * @param Request  $request
     * @param Response $response
     * @param callable $next
     *
     * @return Response|null
     */
    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        $error = $this->getAuthenticationErrorResponse($request, $response);

        if ($error == null) {
            return $next($request, $response);
        } else {
            return $error;
        }
    }


    /**
     * Tests the request to see if it meets authentication criteria.
     *
     * If it fails, an appropriate error response is returned
     *
     * If it is valid, null is returned
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return Response|null
     */
    protected function getAuthenticationErrorResponse(Request $request, Response $response): Response
    {
        $authToken = null;

        // Check for a query param override
        $queryToken = $request->getQueryParam('@auth', null);
        if ($queryToken) {
            $authToken = $queryToken;
        }

        // If no query param token, check for the authorization header
        if (!$authToken) {

            if (!$request->hasHeader('Authentication')) {
                return $this->getAuthenticationRequiredResponse($response);
            }

            $hdrToken = $request->getHeader('Authentication');

            // Header must be in the format 'custom-digest <token>'
            $words = explode(' ', $hdrToken, 2);
            if (sizeof($words) != 2 || $words[0] != 'custom-digest') {
                return $this->getInvalidCredentialsResponse($response);
            }

            try {
                Auth::validateToken($words[1], $request, $this->validMins);
            } catch (\AssertionError $err) {
                return $this->getInvalidCredentialsResponse($response, $err);
            }
        }
        return null;
    }


    /**
     * Validate an authorisation token, throwing assertion errors
     * if it is not valid
     *
     * @param string  $token
     * @param Request $request
     *
     * @throws \AssertionError
     */
    protected function validateToken(string $token, Request $request)
    {
        // Token must be in format <timestamp>:<digest>
        $parts = explode(':', $token, 2);

        assert(sizeof($parts) == 2, "Malformed token");

        assert(Validate::positiveInt($parts[0]), "Timestamp is not an int");
        $timestamp = intval($parts[0]);

        $timeDelta = abs(\time() - $timestamp);
        assert($timeDelta < $this->validMins * 60, "Timestamp out of valid delta");

        assert(Validate::hexString($parts[1]), "Digest is not a hex string");
        assert(sizeof($parts[1]) == 64, "Digest is not a sha256 hash");
        $digest = $parts[1];

        // Construct our own digest from the request to check against
        $controlDigest = Auth::generateToken($request, $timestamp);
        assert($digest == $controlDigest, "Digest is not valid");
    }


    /**
     * Returns an HTTP error response indicating that authentication is required
     *
     * @param Response $response
     *
     * @return Response
     */
    protected function getAuthenticationRequiredResponse(Response $response): Response
    {
        return $response
            ->withStatus(401, 'Authentication required');
    }

    /**
     * Returns an HTTP error response indicating that the supplied authentication
     * credentials are invalid
     *
     * @param Response             $response
     * @param \AssertionError|null $err
     *
     * @return Response
     */
    protected function getInvalidCredentialsResponse(Response $response, \AssertionError $err = null): Response
    {
        return $response
            ->withStatus(401, $err ? $err->getMessage() : 'Invalid credentials');
    }


}