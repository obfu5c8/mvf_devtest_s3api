<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Middleware;

use Slim\Http\Body;
use \Slim\Http\Request;
use \MVF\API\Http\Response;

use \MVF\API\ContentTypes\IContentType;

/**
 * Class ContentTypeHandler
 * ========================
 *
 *  Handles response content encoding for all requests.
 *  This allows endpoint resources to return structured data
 *  objects that are then encoded into the content-type that
 *  the client requested.
 *
 *  The client requests the content-type using the HTTP `Content-Type`
 *  header as per the HTTP spec, but the `@format` query parameter override is
 *  also provided for clients that are unable to access the headers
 *  of their request.
 *
 *  Variable response formats
 *  -------------------------
 *  The client can request that data be returned in a specific format by either
 *  specifying the desired MIME type in the request's `Accept` header, or
 *  by specifying the `@format` query parameter. The `@format` query parameter
 *  takes precidence over the `Accept` header, so if both exist, the query
 *  parameter will be used to determine response format.
 *
 *  Content-Type header | @format param | Description
 *  --------------------|---------------|-------------------------------------
 *  application/json    |     json      | Encodes the response data as JSON
 *  application/yaml    |     yaml      | Encodes the response data as YAML
 *                      |     jsonp     | Returns a jsonp-wrapped payload
 *
 *  As per HTTP spec, clients can specify multiple MIME-types in one or more
 *  `Accept` headers, along with weightings for those options. If this is the
 *  case, the highest-weighted compatible response format will be chosen.
 *
 *  If no exact matching MIME types are found, but the client specifies an
 *  acceptible MIME type of '*\*', then JSON will be returned by default.
 *
 *  If neither a `Content-Type` header or `@format` query parameter is supplied,
 *  then the default `json` format will be used.
 *
 *  JSONP Responses
 *  ---------------
 *  To facilitate web-browser clients that cannot (for whatever archaic reason)
 *  form proper HTTP requests, JSONP requests are also supported. When making
 *  a JSONP request, the `@format` query parameter should be set to `jsonp` and
 *  and optional `@callback` parameter can be supplied as the name of the function
 *  with which to wrap the response payload. If the `@callback` parameter is omitted,
 *  the wrapper function will be named `callback`.
 *
 *  As an additional helper, you can also specify the `@callback-id` parameter which
 *  will return its value as part of the function payload.
 *
 *  __Example__ `?@callback=respond`
 *  ```js
 *  respond({ "foo": "bar" });
 *  ```
 *
 *  __Example__ `?@callback=respond&@callback-id=123
 *  ```js
 *  respond('123', { "foo": "bar" });
 *  ```
 */
class ContentTypeHandler
{


    /**
     * Supported Content types
     *
     * @var IContentType[]
     */
    protected $contentTypes = [];


    /**
     * ContentTypeHandler constructor.
     *
     * @param IContentType[] $contentTypes
     */
    public function __construct($contentTypes = [])
    {
        $this->contentTypes = $contentTypes;
    }


    /**
     * Middleware executable
     *
     * Checks the request data to decide on an appropriate response format.
     * If an invalid request is detected, then an error Response is returned
     * before the endpoint is processed.
     *
     * Otherwise it continues the execution chain until a response is ready,
     * which is then encoded in the desired format before being returned.
     *
     * @param Request  $request  HTTP Request
     * @param Response $response HTTP Response to be prepared
     * @param callable $next     Next handler in chain
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next): Response
    {
        $encoder = null;

        // Check for a `@format` query param to use as an override
        $format = $request->getQueryParam('@format');
        if ($format) {
            $encoder = $this->findContentTypeEncoderForFormat($format);
            if (!$encoder) {
                return $this->generateUnsupportedFormatResponse($format, $response);
            }
        }

        if (!$encoder) {
            // Check request `Accept` headers for a compatible MIME type
            $encoder = $this->findAcceptableContentTypeEncoder($request);
            if (!$encoder) {
                return $this->generateNoAcceptableMIMEsResponse($response);
            }
        }

        // To reach this point we have definitively found an acceptable
        // response format, so let's continue the handler chain to get
        // some response data
        /* @type Response $response */
        $response = $next($request, $response);

        // Now we encode the response data using whatever format was
        // selected earlier


        if (!$response->hasData()) {
            return $response;
        } else {
            $body = new Body(fopen('php://temp', 'r+'));
            $body->write($encoder->encode($response->getData(), $request));

            return $response
                ->withHeader('Content-Type', $encoder->getMimeType())
                ->withBody($body);
        }
    }


    /**
     * Process the request's Accept headers (if any) and
     * use the weighted values to select the most appropriate content
     * type to respond with.
     *
     * If no Accept headers are present, it is assumed that the client
     * does not care about response format and so we shall use our
     * default format.
     *
     * @param Request $request The HTTP request
     *
     * @return IContentType|null
     */
    protected function findAcceptableContentTypeEncoder(Request $request)
    {
        $headers = $request->getHeader('Accept');

        // If no Accept headers, return our default encoder
        if (!$headers) {
            return $this->contentTypes[0];
        }

        // Extract MIME & weightings from each headers and make a list
        $acceptableTypes = [];
        foreach ($headers as $header) {
            $items = explode(',', $header);

            foreach ($items as $item) {
                $q = 1;    // No q value implies 1.0
                $bits = explode(';', $item);

                $mime = trim($bits[0]);

                try {
                    if (sizeof($bits) == 2) {
                        if (substr(trim($bits[1]), 0, 1) == 'q') {
                            $q = floatval(explode('=', $bits[1]))[1];
                        }
                    }
                } catch (\Exception $err) {
                    // Something weird in the header. Just ignore it
                }

                $acceptableTypes[$mime] = $q;
            }
        }

        // Sort the list of MIMEs according to their q value
        arsort($acceptableTypes);

        // Now iterate through and find one that matches our supported formats
        foreach ($acceptableTypes as $mime => $q) {
            $encoder = $this->findContentTypeEncoderForMimeType($mime);
            if ($encoder) {
                return $encoder;
            }
        }

        return null;
    }


    /**
     * Attempts to match a format value to a supported content type.
     * Returns null if a match is not found
     *
     * @param $format {string} Format value to match
     *
     * @returns IContentType|null
     */
    protected function findContentTypeEncoderForFormat(string $format)
    {
        foreach ($this->contentTypes as $type) {
            if ($type->getFormatKey() == $format) {
                return $type;
            }
        }
        return null;
    }

    /**
     * Attempts to match a mime type to a supported content type.
     * Returns null if a match is not found
     *
     * @param $mime {string} MIME type
     *
     * @return IContentType|null
     */
    protected function findContentTypeEncoderForMimeType(string $mime)
    {
        foreach ($this->contentTypes as $type) {
            if ($type->getMimeType() == $mime) {
                return $type;
            }
        }
        return null;
    }


    /**
     * Creates a Response object that signifies that the requested
     * content format is not supported
     *
     * @param string   $badFormat The format that is not supported
     * @param Response $response  The current response data
     *
     * @returns Response
     */
    protected function generateUnsupportedFormatResponse(string $badFormat, Response $response): Response
    {
        $supported = implode("', '",
            array_map(function (IContentType $x) { return $x->getFormatKey(); }, $this->contentTypes));
        return $response
            ->withStatus(400, 'Unsupported @format query value')
            ->write("Supported values are ['" . $supported . "]'");
    }

    /**
     * Creates a Response object that signifies that no acceptable MIME types
     * are available for the response format
     *
     * @param Response $response
     *
     * @returns Response
     */
    protected function generateNoAcceptableMIMEsResponse(Response $response): Response
    {
        $supported = implode("', '",
            array_map(function (IContentType $x) { return $x->getMimeType(); }, $this->contentTypes));
        return $response
            ->withStatus(406, 'No acceptable representations available')
            ->write("Supported MIME types are ['" . $supported . "']");
    }


}