<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Middleware;


use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ResponseBrandingHandler
 * @package MVF\API\Middleware
 */
class ResponseBrandingHandler
{


    /**
     * Middleware executable
     *
     * Applies vanity tweaks to responses
     *
     * @param Request  $request
     * @param Response $response
     * @param callable $next
     *
     * @return mixed
     */
    public function __invoke( Request $request, Response $response, callable $next): Response
    {
        /** @var Response $response */
        $response = $next($request, $response);

        return $response
            ->withHeader('Server', "MVF Data API v1");
    }
}