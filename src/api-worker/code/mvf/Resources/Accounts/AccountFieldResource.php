<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\Accounts;

use MVF\API\Datastore\Datastore;
use MVF\API\Http\Response;
use MVF\API\Resources\Base\ReadOnlyResource;
use MVF\API\Util\Validate;
use Slim\Http\Request;


/**
 * Class AccountFieldResource
 *
 * @package MVF\API\Resources\Accounts
 */
class AccountFieldResource extends ReadOnlyResource
{
    /**
     * Retrieve a single Account resource by guid
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function get(Request $request, Response $response, array $args = []): Response
    {
        $account_guid = $args['account_guid'];
        $field = $args['field'];

        if (!Validate::guid($account_guid)) {
            return $response
                ->withStatus(404, "Account not found")
                ->withData([
                    "error" => "Account not found",
                    "description" => sprintf("No account found with guid '%s'", $account_guid)
                ]);
        }

        $account = Datastore::getAccount($account_guid);

        if (!$account) {
            return $response
                ->withStatus(404, "Account not found")
                ->withData([
                    "error" => "Account not found",
                    "description" => sprintf("No account found with guid '%s'", $account_guid)
                ]);
        }

        if (!array_key_exists($field, $account)) {
            return $response
                ->withStatus(404, "Field not found")
                ->withData([
                    "error" => "Field not found",
                    "description" => sprintf("Account field '%s' does not exist for account '%s'", $field,
                        $account_guid)
                ]);
        }

        return $response->withData($account[$field]);

    }


}