<?php
/**
 * @copyright 2017 Alan Pich
 */
namespace MVF\API\Resources\Accounts;

use MVF\API\Datastore\Datastore;
use MVF\API\Http\Response;
use MVF\API\Resources\Base\ReadOnlyResource;
use MVF\API\Resources\Base\SubResource;
use MVF\API\Util\Validate;
use Slim\Http\Request;

/**
 * Class AccountResource
 *
 * @package MVF\API\Resources\Accounts
 */
class AccountResource extends ReadOnlyResource
{
    /**
     * AccountResource constructor.
     *
     * @param array $subResources  List of sub-resources for this endpoint
     */
    public function __construct($subResources = [])
    {
        parent::__construct([
            new SubResource("/{field}", new AccountFieldResource())
        ]);
    }


    /**
     * Get a single Account object by guid
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function get(Request $request, Response $response, array $args = []) : Response
    {
        $account_guid = $args['account_guid'];

        if (!Validate::guid($account_guid)) {
            return $this->get404Response($response);
        }

        $data = Datastore::getAccount($account_guid);

        if (!$data) {
            return $this->get404Response($response);
        }

        $timestamp = $data['last_updated'];
        unset($data['last_updated']);

        return $response
            ->withLastModifiedTimestamp($timestamp)
            ->withData($data);

    }


    /**
     * Return a prepared HTTP 404 Not Found error response
     *
     * @param Response $response
     *
     * @return Response
     */
    protected function get404Response(Response $response) : Response
    {
        return $response
            ->withStatus(404, "Account not found")
            ->withData([
                "error" => "Account not found",
                "description" => sprintf("No account found")
            ]);
    }
}