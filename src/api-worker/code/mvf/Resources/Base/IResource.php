<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\Base;


/**
 * Interface IResource
 *
 * @package MVF\API\Resources\Base
 */
interface IResource
{

    /**
     * Register this resource the the app's router
     * This method is responsible for calling register()
     * for any sub-resources as well
     *
     * @param \Slim\App $app  The app to register resource to
     * @param string    $path URL path to register resource at
     */
    public function register(\Slim\App $app, string $path);

}