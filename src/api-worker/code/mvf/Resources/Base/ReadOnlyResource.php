<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\Base;


use MVF\API\Http\Response;
use Slim\Http\Request;

/**
 * Class ReadOnlyResource
 *
 * @package MVF\API\Resources\Base
 */
abstract class ReadOnlyResource extends Resource
{
    /**
     * Register this resource the the app's router
     * This method is responsible for calling register()
     * for any sub-resources as well
     *
     * @param \Slim\App $app  The app to register resource to
     * @param string    $path URL path to register resource at
     */
    public function register(\Slim\App $app, string $path)
    {
        $app->options($path, [$this, 'handleOPTIONS']);
        $app->get($path, [$this, 'handleGET']);

        foreach ($this->subResources as $sub) {
            $subPath = $path . $sub->getPath();
            $sub->getResource()->register($app, $subPath);
        }
    }


    /**
     * Handle an HTTP GET request
     *
     * Don't override this method, implement abstract method get() instead
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function handleGET(Request $request, Response $response, array $args = []): Response
    {
        return $this->get($request, $response, $args);
    }


    /**
     * Handle an HTTP OPTIONS request
     *
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function handleOPTIONS(Request $request, Response $response): Response
    {
        return $response
            ->withHeader('Allowed', 'HEAD,GET,OPTIONS');
    }


    /**
     * Handle an HTTP GET request.
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    abstract public function get(Request $request, Response $response, array $args = []): Response;


}