<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\Base;


/**
 * Class Resource
 *
 * @package MVF\API\Resources\Base
 */
abstract class Resource implements IResource
{

    /** @var SubResource[] */
    protected $subResources = [];


    /**
     * Resource constructor.
     *
     * @param SubResource[]|null $subResources
     */
    public function __construct(array $subResources = [])
    {
        $this->subResources = $subResources;
    }


    /**
     * Register this resource the the app's router
     * This method is responsible for calling register()
     * for any sub-resources as well
     *
     * @param \Slim\App $app  The app to register resource to
     * @param string    $path URL path to register resource at
     */
    abstract public function register(\Slim\App $app, string $path);


}