<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\Base;


/**
 * Class SubResource
 *
 * @package MVF\API\Resources\Base
 */
class SubResource
{
    /** @var string */
    private $path;

    /** @var Resource */
    private $resource;

    /**
     * SubResource constructor.
     *
     * @param string   $path
     * @param Resource $resource
     */
    public function __construct(string $path, Resource $resource)
    {
        $this->path = $path;
        $this->resource = $resource;
    }

    /**
     * Returns the path this resource should be mounted at relative to parent
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Returns the resource to be mounted under parent
     *
     * @return Resource
     */
    public function getResource(): Resource
    {
        return $this->resource;
    }


}