<?php
/**
 * @copyright (c) 2017, Alan Pich
 */

namespace MVF\API\Resources\CustomerAccounts;

use MVF\API\Combinations\Combination;
use MVF\API\Combinations\Entry;
use MVF\API\Combinations\Result;
use MVF\API\Datastore\Datastore;
use MVF\API\Http\Response;
use MVF\API\Resources\Base\ReadOnlyResource;
use MVF\API\Util\Validate;

use Slim\Http\Request;
use Math_Combinatorics;

/**
 * Class CustomerAccountsCombinationsResource
 *
 * @package MVF\API\Resources\CustomerAccounts
 */
class CustomerAccountsCombinationsResource extends ReadOnlyResource
{

    /**
     * Handle an HTTP GET request.
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function get(Request $request, Response $response, array $args = []): Response
    {
        $customer_guid = $args['customer_guid'];

        if (!Validate::guid($customer_guid) || !Datastore::doesCustomerExist($customer_guid)) {
            return $response
                ->withStatus(404, "Customer not found")
                ->withData([
                    'error' => "Customer not found",
                    'description' => sprintf("No customer found for id '%s'", $customer_guid)
                ]);
        }


        $min = $request->getQueryParam('min', '0');
        if(!Validate::float($min)){
            return $response
                ->withStatus(400, "Invalid min value")
                ->withData([
                    'error' => "Invalid min value",
                    'description' => "Min value must be a valid float"
                ]);
        }

        $max = $request->getQueryParam('max', '1000');
        if(!Validate::float($max)){
            return $response
                ->withStatus(400, "Invalid max value")
                ->withData([
                    'error' => "Invalid max value",
                    'description' => "Max value must be a valid float"
                ]);
        }


        $matches = $this->getBestCombination($customer_guid, $min, $max);

//        die(print_r($matches, true));
        if($matches === null){
            $matches = [];
        }

//        die(print_r($matches,true));

        return $response
            ->withData($matches);

    }




    protected function getBestCombination($customer_guid, $min, $max)
    {
        $accounts = Datastore::getAccountBalancesForCustomerAccounts($customer_guid);

        $dataset = array_map(function($e){
            return new Entry($e['guid'], floatval($e['balance']));
        }, $accounts);

        $n = sizeof($dataset);
	    $r = 1;

	    $iterations = 0;

	    $MC = new Math_Combinatorics();


        $startTime = \microtime(true);

        while ($r <= $n) {
            /** @var Combination[] $combs */
            $combs = array_map(function(array $items){
                return new Combination($items);
            }, $MC->combinations($dataset, $r));


            foreach($combs as $comb) {
                ++$iterations;
                if( $comb->getSum() >= $min && $comb->getSum() <= $max){

                    $time = microtime(true) - $startTime;
//                    die(print_r(new Result($comb->getEntries(), $iterations, $time)));
                    return new Result($comb->getEntries(), $iterations, $time);
                }
            }

            $combs = null;
            ++$r;
        }

        return null;
    }
}