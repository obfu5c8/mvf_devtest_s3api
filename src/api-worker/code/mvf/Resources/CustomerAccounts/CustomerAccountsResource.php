<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Resources\CustomerAccounts;


use AssertionError;
use Exception;
use Slim\Http\Request;

use MVF\API\Datastore\Datastore;
use MVF\API\Datastore\Comparators\Comparator;
use MVF\API\Datastore\Comparators\NumberEQComparator;
use MVF\API\Datastore\Comparators\NumberGTComparator;
use MVF\API\Datastore\Comparators\NumberGTEComparator;
use MVF\API\Datastore\Comparators\NumberLTComparator;
use MVF\API\Datastore\Comparators\NumberLTEComparator;
use MVF\API\Datastore\Comparators\StringContainsComparator;
use MVF\API\Datastore\Comparators\StringEndsWithComparator;
use MVF\API\Datastore\Comparators\StringEqualsComparator;
use MVF\API\Datastore\Comparators\StringStartsWithComparator;
use MVF\API\Http\Response;
use MVF\API\Resources\Base\ReadOnlyResource;
use MVF\API\Resources\Base\SubResource;
use MVF\API\Util\RequestHelper;
use MVF\API\Util\Validate;



/**
 * Class CustomerAccountsResource
 *
 * @package MVF\API\Resources\CustomerAccounts
 */
class CustomerAccountsResource extends ReadOnlyResource
{
    /**
     * CustomerAccountsResource constructor.
     */
    public function __construct()
    {
        parent::__construct([
            new SubResource('/combinations', new CustomerAccountsCombinationsResource())
        ]);
    }


    /**
     * Query accounts tied to a customer identified by $customer_guid
     * Returns a Response with attached array of account GUIDs that
     * match the query criteria.
     *
     * Query parameters can be used to filter the results and set the
     * sorting order - see API docs for details.
     *
     * ?firstname= foo    - exact match 'foo'
     * ?firstname= |foo   - start with 'foo
     * ?firstname= foo|   - ends with 'foo'
     * ?firstname= ~foo   - contains 'foo'
     *
     * ?balance= 123      - == 123
     * ?balance= <123     - < 123
     * ?balance= <=123    - <= 123
     * ?balance= >123     - > 123
     * ?balance= >=123    - >= 123
     *
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function get(Request $request, Response $response, array $args = []): Response
    {
        $customer_guid = $args['customer_guid'];

        if (!Validate::guid($customer_guid) || !Datastore::doesCustomerExist($customer_guid)) {
            return $response
                ->withStatus(404, "Customer not found")
                ->withData([
                    'error' => "Customer not found",
                    'description' => sprintf("No customer found for id '%s'", $customer_guid)
                ]);
        }


        $filter = $this->getFilterComparators($request);
        $sort = $this->getSortFields($request);

        return $response->withData(
            Datastore::getAccountsForCustomer($customer_guid, $filter, $sort)
        );
    }


    /**
     * Using request's query params, gather the filtering comparators to use
     * for this query
     *
     * @param Request $request
     *
     * @return Comparator[]
     */
    protected function getFilterComparators(Request $request): array
    {
        $query_comparators = [];

        // String search params
        $lastName = RequestHelper::getQueryParamArray($request, 'lastname');
        if ($lastName) {
            foreach ($lastName as $val) {
                $query_comparators [] = $this->getStringComparator('lastname', $val);
            }
        }
        $firstName = RequestHelper::getQueryParamArray($request, 'firstname');
        if ($firstName) {
            foreach ($firstName as $val) {
                $query_comparators [] = $this->getStringComparator('firstname', $val);
            }
        }
        $email = RequestHelper::getQueryParamArray($request, 'email');
        if ($email) {
            foreach ($email as $val) {
                $query_comparators [] = $this->getStringComparator('email', $val);
            }
        }

        // Numerical search params
        $balance = RequestHelper::getQueryParamArray($request, 'balance');
        if ($balance) {
            foreach ($balance as $val) {
                $query_comparators [] = $this->getNumberComparator('balance', $val);
            }
        }

        return $query_comparators;
    }


    /**
     * Extract sorting order from request as an array of SQL 'SORT BY' clauses
     *
     * @param Request $request
     *
     * @return string[]
     * @throws Exception
     */
    protected function getSortFields(Request $request): array
    {

        $sortParam = $request->getQueryParam('sort');
        if (!$sortParam) {
            return [];
        }

        $valid_fields = [
            'firstname',
            'lastname',
            'email',
            'phone',
            'balance'
        ];

        $sortFields = [];

        $bits = explode(',', $sortParam);
        foreach ($bits as $field) {
            $asc = true;
            if (substr($field, 0, 1) == '-') {
                $asc = false;
                $field = substr($field, 1);
            }
            if (!in_array($field, $valid_fields)) {
                throw new Exception(sprintf("Invalid sort field '%s'", $field));
                //@TODO - Return a nice error response
            }

            //@TODO: This is lazy - MySQL syntax should be abstracted to a class
            $sortFields [] = $field . ' ' . ($asc ? 'ASC' : 'DESC');
        }

        return $sortFields;

    }


    /**
     * Construct the appropriate string comparator instance for the
     * query parameter of a string type
     *
     * @param string $field
     * @param string $value
     *
     * @return Comparator
     */
    protected function getStringComparator(string $field, string $value): Comparator
    {
        $firstChar = substr($value, 0, 1);


        if ($firstChar == '~') {
            $value = $this->validateString(substr($value, 1));
            return new StringContainsComparator($field, $value);
        } else {
            if ($firstChar == '|') {
                $value = $this->validateString(substr($value, 1));
                return new StringStartsWithComparator($field, $value);
            } else {
                $lastChar = substr($value, -1, 1);
                if ($lastChar == '|') {
                    $value = $this->validateString(substr($value, 0, -1));
                    return new StringEndsWithComparator($field, $value);
                } else {
                    return new StringEqualsComparator($field, $this->validateString($value));
                }
            }
        }
    }


    /**
     * Construct the appropriate query comparator for the query parameter assuming
     * that the value is a number
     *
     * @param string $field
     * @param string $value
     *
     * @return Comparator
     */
    protected function getNumberComparator(string $field, string $value): Comparator
    {
        $firstChar = $value[0];
        $secondChar = $value[1];
        $comparator = null;

        if ($firstChar == '<') {
            if ($secondChar == '=') {
                $num = $this->validateFloat(substr($value, 2));
                return new NumberLTEComparator($field, $num);
            } else {
                $num = $this->validateFloat(substr($value, 1));
                return new NumberLTComparator($field, $num);
            }
        } else {
            if ($firstChar == '>') {
                if ($secondChar == '=') {
                    $num = $this->validateFloat(substr($value, 2));
                    return new NumberGTEComparator($field, $num);
                } else {
                    $num = $this->validateFloat(substr($value, 1));
                    return new NumberGTComparator($field, $num);
                }
            } else {
                $num = $this->validateFloat($value);
                return new NumberEQComparator($field, $num);
            }
        }
    }


    /**
     * Validate a string can be parsed as a legal float value,
     * and return it (still as a string)
     *
     * @param string $val
     *
     * @return string
     * @throws AssertionError if value is not parseable as a float
     */
    protected function validateFloat(string $val): string
    {
        if (!Validate::float($val)) {
            throw new \AssertionError(sprintf("Validation error - %s is not a float", $val));
        }
        return $val;
    }

    /**
     * Validate a string is valid (contains only legal chars),
     * and return it if it is valid
     *
     * @param string $val
     *
     * @return string
     * @throws AssertionError if value is not parseable as a float
     */
    protected function validateString($val): string
    {
        if (!Validate::naivestring($val)) {
            throw new \AssertionError(sprintf("Validation error- %s is not a naiveString", $val));
        }
        return $val;
    }


}