<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Util;

use Slim\Http\Request;

/**
 * Class Auth
 *
 * @package MVF\API\Util
 */
class Auth
{
    /**
     * Generate the custom-digest token used for authentication of requests
     *
     * sha256( <timestamp><http verb><uri path><secret key> )
     *
     * @param Request  $request
     * @param string   $secret
     * @param int|null $timestamp
     *
     * @return string
     */
    public static function generateToken(Request $request, string $secret, int $timestamp = null): string
    {
        if ($timestamp == null) {
            $timestamp = time();
        }

        return hash('sha256', sprintf('%s%s%s%s',
            $timestamp, $request->getMethod(), $request->getUri(), $secret
        ));
    }


    /**
     * Validate a token against a request
     *
     * @param string  $token
     * @param Request $request
     * @param int     $validMins
     * @throws \AssertionError if token is not valid
     */
    public static function validateToken(string $token, Request $request, $validMins = 10)
    {
        // Token must be in format <timestamp>:<digest>
        $parts = explode(':', $token, 2);

        assert(sizeof($parts) == 2, "Malformed token");

        assert(Validate::positiveInt($parts[0]), "Timestamp is not an int");
        $timestamp = intval($parts[0]);

        $timeDelta = abs(\time() - $timestamp);
        assert($timeDelta < $validMins * 60, "Timestamp out of valid delta");

        assert(Validate::hexString($parts[1]), "Digest is not a hex string");
        assert(sizeof($parts[1]) == 64, "Digest is not a sha256 hash");
        $digest = $parts[1];

        // Construct our own digest from the request to check against
        $controlDigest = self::generateToken($request, $timestamp);
        assert($digest == $controlDigest, "Digest is not valid");
    }

}