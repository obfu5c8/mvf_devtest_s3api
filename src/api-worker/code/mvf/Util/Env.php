<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Util;


/**
 * Class Env
 *
 * @package MVF\API\Util
 */
class Env
{

    /**
     * Get an environment variable, with optional default value
     * if the env var is not set or is empty
     *
     * @param string      $var
     * @param string|null $default
     *
     * @return array|false|string
     */
    public static function get(string $var, string $default = null): string
    {
        $val = getenv($var);
        if ($val == null) {
            return $default;
        }
        return $val;
    }

}