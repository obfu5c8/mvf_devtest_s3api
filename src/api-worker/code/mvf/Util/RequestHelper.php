<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Util;


use Slim\Http\Request;

/**
 * Class RequestHelper
 *
 * Utility methods to work around Slim's lack of support for
 * multiple query string params with the same name
 *
 * @package MVF\API\Util
 */
class RequestHelper
{

    /**
     * Return an array of values for query parameter $param
     *
     * @param Request $request The Request
     * @param string  $param   The parameter to get values for
     * @param int     $max     Maximum values to return
     *
     * @return string[]
     */
    public static function getQueryParamArray(Request $request, string $param, $max = 3): array
    {

        $params = self::parseQueryStringForMultipleParams($request, $max);
        if (\key_exists($param, $params)) {
            return $params[$param];
        }
        return [];
    }


    /**
     * Parse a request's query string into an associative array of parameter names
     * containing zero or more values
     *
     * [
     *  'param' => ['multiple','values']
     * ]
     *
     * @param Request $request
     * @param int     $max
     *
     * @return array[]
     */
    public static function parseQueryStringForMultipleParams(Request $request, $max = 3): array
    {
        if ($request->getUri() === null) {
            return [];
        }
        $params = [];
        foreach (explode('&', $request->getUri()->getQuery()) as $pair) {
            if (sizeof($params) > $max) {
                break;
            }
            $bits = explode('=', $pair);
            if (!key_exists($bits[0], $params)) {
                $params[$bits[0]] = [];
            }
            $params[$bits[0]] [] = urldecode($bits[1]);
        }
        return $params;
    }


}