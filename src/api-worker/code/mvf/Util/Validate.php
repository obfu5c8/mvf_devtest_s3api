<?php
/**
 * @copyright 2017 Alan Pich
 */

namespace MVF\API\Util;

/**
 * Class Validate
 *
 * String validation utilities
 *
 * @package MVF\API\Util
 */
class Validate
{

    /**
     * Validate string can be parsed as a legal integer
     *
     * @param string $val
     *
     * @return bool
     */
    static public function int(string $val): bool
    {
        return preg_match('/^-?[0-9]+$/', $val);
    }

    /**
     * Validate a string can be parsed as a positive integer
     *
     * @param string $val
     *
     * @return bool
     */
    static public function positiveInt(string $val): bool
    {
        return preg_match('/^[0-9]+$/', $val);
    }

    /**
     * Validate a string can be parsed as a float
     *
     * @param string $val
     *
     * @return bool
     */
    static public function float(string $val): bool
    {
        return preg_match('/^-?[0-9]+(?:\.[0-9]+)?$/', $val);
    }

    /**
     * Validate a string contains only the narrow set of characters
     * allowed in this exercise
     *
     * @param string $val
     *
     * @return bool
     */
    static public function naivestring(string $val): bool
    {
        return preg_match('/^[a-z0-9_\-\,\@\.]+$/i', $val);
    }

    /**
     * Validate a string contains only characters used to represent
     * hexadecimal numbers
     *
     * @param string $val
     *
     * @return bool
     */
    static public function hexString(string $val): bool
    {
        return preg_match('/^[0-9a-f]+$/i', $val);
    }

    /**
     * Validate a string matches guid format
     *
     * @param string $val
     *
     * @return bool
     */
    static public function guid($val): bool
    {
        return preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i', $val);
    }


}