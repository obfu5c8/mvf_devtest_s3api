<?php
namespace MVF\API\Test\FunctionalTests;


use MVF\API\Test\DummyDatastore;


/**
 * Class AccountsEndpointTest
 * @package MVF\API\Test\FunctionalTests
 */
class AccountsEndpointTest extends TestCase
{
    /**
     *
     */
    const VALID_GUID = '006fe211-0327-4a2f-8888-e00a2409a17a';
    /**
     *
     */
    const VALID_ACCOUNT = [
        'firstname' => 'Tim',
        'lastname'  => 'Armstrong',
        'phone' => '01234 567 890',
        'email' => 'tim@armstrong.com',
        'balance' => -123.45
    ];

    /**
     * @var
     */
    protected $app;


    /**
     *
     */
    protected function setUp()
    {
        $account = self::VALID_ACCOUNT;
        $account['last_updated'] = 1506337213;

        $datastore = new DummyDatastore([],[
            self::VALID_GUID => $account
        ]);

        $this->app = $this->createAppInstance($datastore);
    }


    /**
     *
     */
    public function testValidAccountGuidReturnsObject () {

        $response = $this->dispatch($this->createRequest()
            ->withMethod('GET')
            ->withHeader('Accept', 'application/json')
            ->withPath('/accounts/'.self::VALID_GUID));

        static::assertEquals( $response->getStatusCode(), 200,
            "HTTP Status should be 200 OK");


        $payloadData = json_decode($response->getBody()->getContents(), true);
        static::assertEquals( $payloadData, self::VALID_ACCOUNT,
            "Correct account data should be returned encoded as JSON");

    }


    /**
     *
     */
    public function testInvalidAccountGuidReturns404Error () {
        $response = $this->dispatch($this->createRequest()
            ->withMethod('GET')
            ->withHeader('Accept', 'application/json')
            ->withPath('/accounts/not-a-valid-id'));

        static::assertEquals( $response->getStatusCode(), 404,
            'Invalid GUID should return status 404 Not Found');

    }


}