<?php

namespace MVF\API\Test\FunctionalTests;



use MVF\API\App;
use MVF\API\Cache\Cache;
use MVF\API\Datastore\Datastore;
use MVF\API\Datastore\IDataStore;
use MVF\API\Http\Response;
use MVF\API\Test\DummyCache;
use MVF\API\Test\DummyDatastore;
use MVF\API\Test\DummyRequest;
use MVF\API\Util\Auth;
use Slim\Http\Request;


/**
 * Class TestCase
 * @package MVF\API\Test\FunctionalTests
 */
abstract class TestCase extends \PhpUnit\Framework\TestCase
{
    /** @var  \Slim\App */
    protected $app;

    /**
     * @param IDataStore|null $datastore
     * @param null            $cache
     *
     * @return App
     */
    protected function createAppInstance(IDataStore $datastore=null, $cache=null) {

        if($cache == null){
            $cache = new DummyCache();
        }
        Cache::setImplementation($cache);

        if($datastore == null) {
            $datastore = new DummyDatastore();
        }
        Datastore::setImplementation($datastore);

        return new App();
    }


    /**
     * @param Request        $request
     * @param \Slim\App|null $app
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function dispatch (Request $request, \Slim\App $app=null) {
        if($app == null)
            $app = $this->app;
        if($app == null)
            $app = $this->createAppInstance();

        // Generate the auth token
        $timestamp = time();
        $token = Auth::generateToken($request, $GLOBALS['AUTH_SECRET'], $timestamp);

        $response = $app->process($request, new Response());
        $response->getBody()->rewind();
        return $response;
    }


    /**
     * @return DummyRequest
     */
    protected function createRequest () {
        return new DummyRequest();
    }


}