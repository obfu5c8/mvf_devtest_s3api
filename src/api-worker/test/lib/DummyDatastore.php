<?php
/**
 * Created by PhpStorm.
 * User: alanp
 * Date: 25/09/2017
 * Time: 11:44
 */

namespace MVF\API\Test;


use MVF\API\Datastore\IDataStore;

/**
 * Class DummyDatastore
 * @package MVF\API\Test
 */
class DummyDatastore implements IDataStore
{
    /**
     * @var array
     */
    protected $customers = [];
    /**
     * @var array
     */
    protected $accounts = [];

    /**
     * DummyDatastore constructor.
     *
     * @param array $customers
     * @param array $accounts
     */
    public function __construct(array $customers=[], array $accounts=[])
    {
        $this->customers = $customers;
        $this->accounts = $accounts;
    }


    /**
     * @param $customer_guid
     *
     * @return bool
     */
    public function doesCustomerExist($customer_guid): bool
    {
        return \array_key_exists($customer_guid, $this->customers);
    }

    /**
     * @param       $customer_guid
     * @param array $comparators
     * @param array $sort
     *
     * @return array
     */
    public function getAccountsForCustomer($customer_guid, array $comparators = [], $sort = []): array
    {
        return $this->accounts;
    }

    /**
     * @param $account_guid
     *
     * @return mixed|null
     */
    public function getAccount($account_guid): array
    {
        if(\array_key_exists($account_guid, $this->accounts)){
            return $this->accounts[$account_guid];
        } else {
            return null;
        }
    }
}