<?php

namespace MVF\API\Test;




use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Stream;
use Slim\Http\Uri;


/**
 * Class DummyRequest
 * @package MVF\API\Test
 */
class DummyRequest extends Request
{
    /**
     * DummyRequest constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'GET',
            new Uri('http','example.com'),
            new Headers(), [], [], new Stream(fopen('php://temp', 'r+')),
            []);
    }


    /**
     * @param string $path
     *
     * @return static
     */
    public function withPath (string $path) {
        $uri = $this->getUri()->withPath($path);
        return $this
            ->withUri($uri);
    }


}