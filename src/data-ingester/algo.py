from itertools import combinations


class Entry:
	balance = 0
	guid = 0

	def __init__ ( self, guid, balance ):
		self.guid = guid
		self.balance = balance


	def __int__ ( self ):
		return self.balance


	def __add__ ( self, other ):
		return self.balance + other


	def __radd__ ( self, other ):
		return self.balance + other


class Combination:
	"""
	Represents a combination of one or more Entry instances
	that makes it easy to return the sum of the set
	"""
	_entries = []


	def __init__ ( self, entries=() ):
		self._entries = tuple(entries)


	def sum ( self ):
		return sum(self._entries)


	@property
	def entries ( self ):
		return self._entries


	@property
	def size ( self ):
		return len(self._entries)


	def __int__ ( self ):
		return self.sum()


	def __lt__ ( self, other ):
		return self.sum() < other


	def __gt__ ( self, other ):
		return self.sum() > other


def compute ( dataset, min, max ):
	n = len(dataset)
	matched = None
	r = 1

	iterations = 0

	while r <= n and matched is None:
		combs = [Combination(c) for c in combinations(dataset, r)]
		for comb in combs:
			iterations += 1
			if comb > min and comb < max:
				return iterations, comb
		r += 1

	return iterations, None


##############################################################################
##############################################################################


async def run ():
	import os
	from mvf.ingester.data.db import initialise_database, db_select


	customer_guid = 'be0438bf-8b0d-4c57-913d-fcafb0bb41f0';

	await initialise_database(
			host=os.getenv('MVF_MYSQL_HOST', '127.0.0.1'),
			port=3306,
			name='mvf_api_data',
			user='data_ingestor',
			password='password',
			root_password='password'
	)

	rows = await db_select("""
		SELECT 
			a.guid, 
			a.balance
		FROM
			accounts a
		LEFT JOIN customer_accounts ca
		    ON a.guid = ca.account_guid
		WHERE
			ca.customer_guid = %s
		""", (customer_guid,))

	dataset = [Entry(r['guid'], r['balance']) for r in rows]

	MIN = 199
	MAX = 300

	# dataset = (
	# 	Entry('a', 200),
	# 	Entry('b', 140),
	# 	Entry('c', 22),
	# 	Entry('d', 76),
	# 	Entry('e', 18)
	# )


	print("Looking for fewest combinations of {} values where sum of values > {} and < {}".format(
	      len(dataset), MIN, MAX))

	iterations, match = compute(dataset, MIN, MAX)

	if match is None:
		print("No match found")
	else:
		print(
			"Found a match where SUM({} objects) == {} in {} iterations".format(
				match.size, match.sum(), iterations))
		print("Objects:")
		for entry in match.entries:
			print("  - {}".format(entry.guid))


if __name__ == "__main__":
	import asyncio
	loop = asyncio.get_event_loop()
	loop.run_until_complete(run())


