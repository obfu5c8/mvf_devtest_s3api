
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mvf_api_data
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mvf_api_data`
DEFAULT
	CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mvf_api_data` ;


-- -----------------------------------------------------
-- Table `mvf_api_data`.`customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvf_api_data`.`customers` (
  `guid` VARCHAR(36) NOT NULL COMMENT 'CustomerAccounts GUID',
  `last_updated` INT NOT NULL,
  PRIMARY KEY (`guid`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvf_api_data`.`accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvf_api_data`.`accounts` (
  `guid` VARCHAR(36) NOT NULL COMMENT 'Account GUID',
  `firstname` VARCHAR(128) NOT NULL,
  `lastname` VARCHAR(128) NULL,
  `email` VARCHAR(256) NULL,
  `phone` VARCHAR(32) NULL,
  `balance` DECIMAL(14,2) NULL,
  `last_updated` INT NULL,
  PRIMARY KEY (`guid`),
  FULLTEXT INDEX `idx_account__fulltext` (`firstname`, `lastname`, `email`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mvf_api_data`.`customer_accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mvf_api_data`.`customer_accounts` (
  `customer_guid` VARCHAR(36) NOT NULL,
  `account_guid` VARCHAR(36) NOT NULL,
  PRIMARY KEY (`customer_guid`, `account_guid`),
  INDEX `fk_customer_accounts__account_idx` (`account_guid` ASC),
  CONSTRAINT `fk_customer_accounts__customer`
  FOREIGN KEY (`customer_guid`)
  REFERENCES `mvf_api_data`.`customers` (`guid`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_accounts__account`
  FOREIGN KEY (`account_guid`)
  REFERENCES `mvf_api_data`.`accounts` (`guid`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE USER IF NOT EXISTS 'api_user' IDENTIFIED BY 'password';
GRANT SELECT ON TABLE `mvf_api_data`.* TO 'api_user';

CREATE USER IF NOT EXISTS'data_ingestor' IDENTIFIED BY 'password';
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `mvf_api_data`.* TO 'data_ingestor';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
