import asyncio
import json
import logging
import os

import boto3

from .data.redis import initialise_redis
from .data.db import initialise_database
from .data.s3 import initialise_s3
from .data import update_all_datasets, start_watching_for_changes



POLL_INTERVAL = 10
""" Polling interval (in seconds) to check for data changes.
	:see: .data.start_watching_for_changes
	:note: Set to a stupidly low number here so you can see it working quickly.
"""





async def main():

	## Initialise the S3 API client with required details
	## (these tokens probably shouldnt be hardcoded, but this is a demo)
	initialise_s3(
			bucket_id='mvf-devtest-s3api',
			access_key='AKIAIOAY4QA4EBD2TSUA',
			secret_key='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')

	## Initialise the database, ensuring the schema has been created
	## (these creds probably shouldnt be hardcoded, but this is a demo)
	await initialise_database(
			host=os.getenv('MVF_MYSQL_HOST','127.0.0.1'),
			port=3306,
			name='mvf_api_data',
			user='data_ingestor',
			password='password',
			root_password='password'
	)


	await initialise_redis(
		host=os.getenv('MVF_REDIS_HOST','127.0.0.1')
	)

	## As we have no idea of state right now, fetch all the datasets
	## and check they are up to date in the database
	asyncio.ensure_future(update_all_datasets())

	## Start monitoring the upstream datastore for changes
	asyncio.ensure_future(start_watching_for_changes(10))



def run():
	logging.basicConfig(
			level = logging.INFO,
			format = "[%(levelname)s] %(name)s :: %(message)s"
		)
	loop = asyncio.get_event_loop()
	loop.run_until_complete(main())

	## Mop up any unfinished tasks
	pending = asyncio.Task.all_tasks()
	if len(pending) > 0:
		loop.run_until_complete(asyncio.gather(*pending))



