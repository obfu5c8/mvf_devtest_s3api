import logging
import asyncio
from uuid import UUID

from aiomysql import DictCursor

from .queue import JobQueue
from .s3 import get_upsteam_datasets, get_dataset_data
from .db import db_select_one, get_db
from .redis import obliterate_redis_cache


queue = JobQueue("datasets")


log = logging.getLogger('mvf.data')




async def update_all_datasets ():
	"""
	Update all datasets available.
	
	This involves deleting any customer data that no longer
	exists in the upstream store, updating those that may
	have changed, and adding in any new ones.
	
	In order to avoid race conditions with asynchronous 
	change notifications, all updates are queued rather 
	than processed immediately.
	
	:return: 
	"""

	log.info("Updating all datasets")

	## Get list of objects in the S3 bucket
	for dataset in await get_upsteam_datasets():
		guid = dataset.key.replace('.json','')
		try:
			UUID(guid)
		except:
			log.error("Dataset key '%s' does not appear to contain a guid", dataset.key)
			continue

		last_modified = dataset.last_modified.timestamp()

		existing = await db_select_one("""
			SELECT guid, last_updated FROM customers WHERE guid = %s
			""", (guid,))

		if existing is not None:
			eTs = existing['last_updated']
			if eTs >= last_modified:
				log.info("Dataset %s is up to date", guid)
				continue

		log.info("Need to update %s", guid)
		queue.push(guid, create_update_task(guid, last_modified))







def create_update_task (guid, last_modified):
	"""
	Creates a customer update task.
	
	Creates a customer record if one doesnt already exist,
	always updates the last_modified stamp on the customer record.
	
	The task updates all accounts in the customer dataset, 
	creating them if they don't exist, and updating any changed
	data within them. Also sets the last_modified timestamp on them.
	
	Once data insertion is complete, all orphaned accounts (ones that
	are no longer referenced by any customers) are removed by first 
	killing of any stale relations to this customer, then purging 
	all accounts with no relations at all.
	
	:param guid: 
	:param last_modified: 
	:return: 
	"""


	async def task (log):

		data = await get_dataset_data(guid)
		accounts = data['accounts']

		async with await get_db() as db:
			async with db.cursor(DictCursor) as cur:

				## Create/Update the last_modified stamp on the customer record
				await cur.execute("""
					INSERT INTO `customers` (
						`guid`,
						`last_updated`
					) VALUES ( %s, %s )
					ON DUPLICATE KEY UPDATE
						last_updated = %s						
					""", (guid, last_modified, last_modified))


				## Insert/update all accounts found in the dataset
				def f ( s ): return float(s.replace(',', '') if type(s) is str else s)
				insert_data = [
					(a['id'], a['firstname'], a['lastname'], a['email'],
					 a['telephone'], f(a['balance']), last_modified,
					 a['firstname'], a['lastname'], a['email'], a['telephone'],
					 f(a['balance']), last_modified) \
					for a in accounts
				]
				await cur.executemany("""
					INSERT INTO `accounts` (
						`guid`,
						`firstname`,
						`lastname`,
						`email`,
						`phone`,
						`balance`,
						`last_updated`
					) VALUES (
						%s, %s, %s, %s, %s, %s, %s
					) ON DUPLICATE KEY UPDATE 
						`firstname` = %s,
						`lastname` = %s,
						`email` = %s,
						`phone` = %s,
						`balance` = %s,
						`last_updated` = %s
					""", insert_data)
				log.info("Inserted %s accounts", cur.rowcount)


				## Insert relations between customer and all these accounts
				await cur.executemany("""
					INSERT INTO `customer_accounts` (
						`customer_guid`,
						`account_guid`
					) VALUES (%s,%s) 
					ON DUPLICATE KEY UPDATE 
						customer_guid=customer_guid
					""", [(guid, a['id']) for a in accounts])
				log.info("Inserted %s account relations", cur.rowcount)

				## Delete any relations that are not for these know accounts
				await cur.execute("""
					DELETE FROM `customer_accounts`
					WHERE
						`customer_guid` = %s
					AND 
						`account_guid` NOT IN %s
				""", (guid, [a['id'] for a in accounts]))
				log.info("Deleted %s account relations", cur.rowcount)

				## Finally, delete any accounts that are not referenced by
				## any customers - these no longer exist
				await cur.execute("""
					DELETE FROM `accounts`
					WHERE `guid` NOT IN (
						SELECT `account_guid`
						FROM `customer_accounts`
					)
					""")
				log.info("Deleted %s orphaned accounts", cur.rowcount)

				## Commit the transaction to complete
				await db.commit()
				log.info("Updated %s accounts for customer %s", len(accounts), guid)

				await obliterate_redis_cache()

	return task





async def start_watching_for_changes( poll_time=60 ):
	"""
	Monitor the S3 bucket for any changes to data.
	
	S3 does support broadcasting change (crud) notifications via
	an SQS channel, which would seem the sensible way of achieving 
	real(ish)-time updates, but since I have no admin access to the 
	S3 bucket in order to enable this feature, we'll have to make 
	do with polling...	
	"""
	while True:
		await asyncio.sleep(poll_time)
		await update_all_datasets()




