from os import path, read
import aiomysql
import logging

## Path to database schema file
import asyncio
from aiomysql import DictCursor
from pymysql import OperationalError


SCHEMA_SRC = path.normpath(path.join(path.realpath(__file__), '..','..','..','..','data','schema.sql'))

## Database Credentials
##  These should be stored in envvar rather than hardcoded,
##  but this is a demo, so hey-ho
MYSQL_DB = "mvf_api_data"
MYSQL_HOST = '127.0.0.1'
MYSQL_PORT = 3306
MYSQL_USER = "data_ingestor"
MYSQL_PASS = "password"
MYSQL_ROOT_PASS = "password"


log = logging.getLogger('mvf.db')


async def initialise_database (
		host='localhost',
		port=3306,
		name=None,
		user=None,
		password=None,
		root_password=None

):
	"""
	Sets up the database ready for use.
	
	Runs an idempotent SQL script (as root) that creates all
	the tables, relations, indexes and users we need to run
	the application.
	"""
	global MYSQL_DB, MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASS, MYSQL_ROOT_PASS
	MYSQL_DB = name
	MYSQL_HOST = host
	MYSQL_PORT = port
	MYSQL_USER = user
	MYSQL_PASS = password
	MYSQL_ROOT_PASS = root_password


	log.info("Initialising database schema")

	sql = ''
	with open(SCHEMA_SRC) as f:
		sql = f.read()
	log.debug("Loaded schema from %s", SCHEMA_SRC)


	log.debug("Connecting to db as root")
	connected = False
	while not connected:
		try:
			async with await aiomysql.connect(
					host=MYSQL_HOST, port=MYSQL_PORT,
			        user='root', password=MYSQL_ROOT_PASS) as db:

				log.debug("Running schema import")
				async with db.cursor() as cur:
					await cur.execute(sql)
					await cur.execute("COMMIT;")
				log.debug("Schema import complete")
			connected = True
		except OperationalError:
			log.warning("Unabled to reconnect, trying again in 2 seconds")
			await asyncio.sleep(2)



_db_pool = None  # type: aiomysql.Pool
async def get_db ():
	"""

	:return: 
	:rtype:  Awaitable[aiomysql.connection.Connection]
	"""
	global _db_pool
	if _db_pool is None:
		_db_pool = await aiomysql.create_pool(
				host=MYSQL_HOST, port=MYSQL_PORT,
				user=MYSQL_USER, password=MYSQL_PASS,
				db=MYSQL_DB,
				loop=asyncio.get_event_loop(),
				charset='utf8',
				use_unicode=True
		)
	return _db_pool.acquire()



async def db_select (sql, params=()):
	async with await get_db() as db:
		async with db.cursor(DictCursor) as cur:
			await cur.execute(sql, params)
			rows = await cur.fetchall()
			await cur.execute("COMMIT;")
	return rows


async def db_select_one (sql, params=()):
	sql += ' LIMIT 1'
	async with await get_db() as db:
		async with db.cursor(DictCursor) as cur:
			await cur.execute(sql, params)
			row = await cur.fetchone()
			await cur.execute("COMMIT;")
	return row
