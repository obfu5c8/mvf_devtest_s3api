import collections
import logging

import asyncio



class JobQueue:


	def __init__ (self, name='default'):
		self.name = name
		self._jobs = collections.deque()
		self._guids = []
		self._running = False
		self._log = logging.getLogger('mvf.q.'+name)

	@property
	def length(self):
		return len(self._jobs)


	def push (self, guid, task, args=()):
		if guid in self._guids:
			self._log.debug("Task %s already in queue", guid)
			return

		self._jobs.append((guid,task, args))
		self._guids.append(guid)

		self._log.info("Queued job %s", guid)

		if not self._running:
			self._log.info("Waking up to process jobs")
			asyncio.ensure_future(self._start())


	async def _start(self):
		self._running = True
		while self.length > 0:
			## Remove the task from the queue
			guid,task,args = self._jobs.popleft()
			self._guids.remove(guid)

			self._log.info("Starting job %s", guid)

			joblog = logging.getLogger(self._log.name+'.job-'+guid)
			try:
				await task(joblog, *args)
				self._log.info("Completed job %s", guid)
			except:
				self._log.exception("Job %s failed", guid)

		self._log.info("No more jobs. Going to sleep")
		self._running = False


