from redis import Redis
import logging


log = logging.getLogger('mvf.redis')


_redis = None

async def initialise_redis (host='127.0.0.1'):
	global _redis
	_redis = Redis(host=host)



async def obliterate_redis_cache ():
	"""
	Obliterates the redis cache of sql records.

	This function is a complete cop-out and no time or effort has been 
	put in to make it pleasant. A more intelligent & less agressive, 
	selective cache-busting mechanism could and should be devised but 
	is out of scope for this exercise.

	:return: 
	"""
	global _redis
	try:
		_redis.flushall()
		log.info("Cache obliterated")

	except:
		log.exception("Failed to obliterate redis cache")
