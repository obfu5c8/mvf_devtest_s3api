import json

import boto3
import logging


_AWS_BUCKET_ID = None


log = logging.getLogger('mvf.s3')


_s3 = None
def initialise_s3(
		bucket_id=None,
		access_key=None,
		secret_key=None):
	global _AWS_BUCKET_ID, _s3
	_AWS_BUCKET_ID = bucket_id
	_s3 = boto3.resource('s3',
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_key,
                )
	log.info("AWS client initialised")





async def get_upsteam_datasets():
	bucket = _s3.Bucket(_AWS_BUCKET_ID)
	return bucket.objects.all()




async def get_dataset_data (guid):
	file = guid+".json"
	try:
		obj = _s3.Object(_AWS_BUCKET_ID, file)
		data = obj.get()['Body'].read().decode('utf-8')
		return json.loads(data)
	except:
		log.error("Failed to get data from s3 object")
		raise
