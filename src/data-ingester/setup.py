#!/usr/bin/env python

from distutils.core import setup


import sys
if sys.version_info < (3,6):
    sys.exit('Sorry, Python < 3.6 is not supported')


setup(name='mvf-data-ingester',
      version='1.0',
      description='Data Ingester for MVF API demo',
      author='Alan Pich',
      author_email='alan.pich@gmail.com',
      packages=['mvf.ingester'],
      entry_points={
          'console_scripts': [
              'mvf-ingester = mvf.ingester:run'
          ]
      },
      install_requires=[
          'aiohttp',
          'aiomysql',
          'boto3',
          'redis'
      ]
     )
